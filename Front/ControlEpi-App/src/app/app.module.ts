import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from 'ngx-spinner';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { TabsModule } from 'ngx-bootstrap/tabs';
//consumir api
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
//import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SetoresComponent } from './components/setores/setores.component';
import { EpisComponent } from './components/epis/epis.component';
import { ColaboradoresEpisComponent } from './components/colaboradores-epis/colaboradores-epis.component';
import { NavComponent } from './shared/nav/nav.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TituloComponent } from './shared/titulo/titulo.component';
import { UserComponent } from './components/user/user.component';
import { EmpresaComponent } from './components/user/empresa/empresa.component';
import { LoginComponent } from './components/user/login/login.component';
import { PerfilComponent } from './components/user/perfil/perfil.component';
import { RegistrationComponent } from './components/user/registration/registration.component';
import { ColaboradoresComponent } from './components/colaboradores/colaboradores.component';
import { ColaboradorDetalheComponent } from './components/colaboradores/colaborador-detalhe/colaborador-detalhe.component';
import { ColaboradorListaComponent } from './components/colaboradores/colaborador-lista/colaborador-lista.component';
import { DateTimeFormatPipe } from './helpers/DateTimeFormat.pipe';
import { EpiDetalheComponent } from './components/epis/epi-detalhe/epi-detalhe.component';
import { EpiListaComponent } from './components/epis/epi-lista/epi-lista.component';
import { SetorDetalheComponent } from './components/setores/setor-detalhe/setor-detalhe.component';
import { SetorListaComponent } from './components/setores/setor-lista/setor-lista.component';
import { JwtInterceptor } from './interceptors/jwt.interceptor';
import { HomeComponent } from './components/home/home.component';


@NgModule({
  declarations: [
    AppComponent,
    SetoresComponent,
    EpisComponent,
    ColaboradoresEpisComponent,
    NavComponent,
    TituloComponent,
    UserComponent,
    EmpresaComponent,
    LoginComponent,
    PerfilComponent,
    RegistrationComponent,
    ColaboradoresComponent,
    ColaboradorDetalheComponent,
    ColaboradorListaComponent,
    DateTimeFormatPipe,
    HomeComponent,
    EpiDetalheComponent,
    EpiListaComponent,
    SetorDetalheComponent,
    SetorListaComponent
  ],
  imports: [
    //AppRoutingModule
    BrowserModule,
    AppRoutingModule,
    //consumir api
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    CollapseModule.forRoot(),
    TooltipModule.forRoot(),
    BsDropdownModule.forRoot(),
    PaginationModule.forRoot(),
    ModalModule.forRoot(),
    TabsModule.forRoot(),
    NgxSpinnerModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
      progressBar: true
    })
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
  //, schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
