import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ColaboradorDetalheComponent } from './components/colaboradores/colaborador-detalhe/colaborador-detalhe.component';
import { ColaboradorListaComponent } from './components/colaboradores/colaborador-lista/colaborador-lista.component';
import { ColaboradoresComponent } from './components/colaboradores/colaboradores.component';
import { EpiDetalheComponent } from './components/epis/epi-detalhe/epi-detalhe.component';
import { EpiListaComponent } from './components/epis/epi-lista/epi-lista.component';
import { EmpresaComponent } from './components/user/empresa/empresa.component';
import { PerfilComponent } from './components/user/perfil/perfil.component';
import { SetorListaComponent } from './components/setores/setor-lista/setor-lista.component';
import { SetorDetalheComponent } from './components/setores/setor-detalhe/setor-detalhe.component';
import { SetoresComponent } from './components/setores/setores.component';
import { RegistrationComponent } from './components/user/registration/registration.component';
import { LoginComponent } from './components/user/login/login.component';
import { UserComponent } from './components/user/user.component';
import { EpisComponent } from './components/epis/epis.component';
import { AuthGuard } from './guard/auth.guard';
import { HomeComponent } from './components/home/home.component';
import { NavComponent } from './shared/nav/nav.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path:'',
    runGuardsAndResolvers: 'always',
    canActivate: [AuthGuard],
    children: [
      { path: 'user', redirectTo: 'user/perfil' },
      {
        path: 'user/perfil', component: PerfilComponent,
        //children:[
        //  { path: 'detalhe/:id', component: PerfilDetalheComponent },
        //  { path: 'detalhe', component: PerfilDetalheComponent },
        //]
      },
      {
        path: 'user/empresa', component: EmpresaComponent
      },
      {
        path: 'setores', component: SetoresComponent,
        children:[
          { path: 'detalhe/:id', component: SetorDetalheComponent },
          { path: 'detalhe', component: SetorDetalheComponent },
          { path: 'lista', component: SetorListaComponent },
        ]
      },
      { path: 'setores', redirectTo: 'setores/lista' },
      {
        path: 'colaboradores', component: ColaboradoresComponent,
        children:[
          { path: 'detalhe/:id', component: ColaboradorDetalheComponent },
          { path: 'detalhe', component: ColaboradorDetalheComponent },
          { path: 'lista', component: ColaboradorListaComponent },
        ]
      },
      { path: 'colaboradores', redirectTo: 'colaboradores/lista' },
      {
        path: 'epis', component: EpisComponent,
        children:[
          { path: 'detalhe/:id', component: EpiDetalheComponent },
          { path: 'detalhe', component: EpiDetalheComponent },
          { path: 'lista', component: EpiListaComponent },
        ]
      },
      { path: 'epis', redirectTo: 'epis/lista' }
    ]
  },
  {
    path: 'user', component: UserComponent,
    children: [
      { path: 'login', component: LoginComponent },
      { path: 'registration', component: RegistrationComponent }
    ]
  },
  { path: 'home', component: HomeComponent },
  //{ path: '**', redirectTo: 'home', pathMatch: 'full' },
];

@NgModule({
  //imports: [RouterModule.forRoot(routes, { useHash: true } )],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
