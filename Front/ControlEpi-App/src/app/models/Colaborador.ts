import { ColaboradorEpi } from "./ColaboradorEpi";

export interface Colaborador {
  id: number;
  setorId: number;
  nome: string;
  cpf: string;
  rg: string;
  endereco: string;
  municipio: string;
  uf: string;
  cep: string;
  telefone: string;
  email: string;
  colaboradoresEpis: ColaboradorEpi[];
}
