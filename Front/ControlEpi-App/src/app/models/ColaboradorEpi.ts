export interface ColaboradorEpi {
  id: number;
  colaboradorId: number;
  colaboradorNome: string;
  epiId: number;
  epiNome: string;
  dataHoraEntrega: Date;
  devolvido: boolean;
}
