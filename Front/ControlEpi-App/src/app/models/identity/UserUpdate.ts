export class UserUpdate {
  titulo: string;
  userName: string;
  nome: string;
  email: string;
  phoneNumber: string;
  funcao: string;
  descricao: string;
  password: string;
  token: string;
  perfilImgUrl: string;
}
