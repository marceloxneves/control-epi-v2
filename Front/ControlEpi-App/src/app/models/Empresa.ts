export interface Empresa {
  id: number;
  razaoSocial: string;
  cnpj: string;
  inscrEst: string;
  endereco: string;
  municipio: string;
  uf: string;
  cep: string;
  telefone: string;
  email: string;
}
