import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Setor } from 'src/app/models/Setor';
import { SetorService } from 'src/app/services/setor.service';

@Component({
  selector: 'app-setor-detalhe',
  templateUrl: './setor-detalhe.component.html',
  styleUrls: ['./setor-detalhe.component.scss']
})
export class SetorDetalheComponent implements OnInit {

  form!: FormGroup;
  setor = {} as Setor;
  setorId: number;
  tipoSalvamento = 'post';

  get f(): any {
    return this.form.controls;
  }

  constructor(
    private fb: FormBuilder,
    private setorService: SetorService,
    private router: Router,
    private activatedRouter: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.carregarSetor();
    this.validation();
  }

  public carregarSetor(): void {
    this.setorId = +this.activatedRouter.snapshot.paramMap.get('id');

    if(this.setorId !== null && this.setorId !== 0){
      this.spinner.show();

      this.tipoSalvamento = 'put';

      this.setorService.getSetorById(this.setorId).subscribe({
        next: (setor: Setor) => {
          this.setor = {...setor};
          this.form.patchValue(this.setor);
        },
        error: (error: any) => {
          this.spinner.hide();
          this.toastr.error('Falha ao carregar Setor', 'Falha');
          console.error(error);
        },
        complete: () => this.spinner.hide()
      });
    }
  }

  public salvarAlteracao(): void {
    this.spinner.show();

    if(this.form.valid) {
      this.setor =
        this.tipoSalvamento === 'post'
          ? { ...this.form.value }
          : { id: this.setor.id, ...this.form.value };

      this.setorService[this.tipoSalvamento](this.setor).subscribe(
        (setorRetorno: Setor) => {
          this.toastr.success('Setor salvo com Sucesso!', 'Sucesso');
          this.router.navigate([`setores/detalhe/${setorRetorno.id}`]);
        },
        (error: any) => {
          console.error(error);
          this.toastr.error('Error ao salvar setor', 'Erro');
        }
      ).add(() => this.spinner.hide());


    }
  }

  public validation(): void {
    this.form = this.fb.group({
      nome: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(200)]]
    });
  }

  onSubmit(): void {
    if(this.form.invalid) {
      return;
    }
  }

  public resetForm(event: any): void {
    event.preventDefault();
    this.form.reset();
  }

}
