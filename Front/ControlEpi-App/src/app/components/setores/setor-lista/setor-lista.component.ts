import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { debounceTime, Subject } from 'rxjs';
import { PaginatedResult, Pagination } from 'src/app/models/Pagination';
import { Setor } from 'src/app/models/Setor';
import { SetorService } from 'src/app/services/setor.service';

@Component({
  selector: 'app-setor-lista',
  templateUrl: './setor-lista.component.html',
  styleUrls: ['./setor-lista.component.scss']
})
export class SetorListaComponent implements OnInit {

  modalRef?: BsModalRef;
  public setores: Setor[] = [];
  //public setoresFiltrados: Setor[] = [];
  // private _filtroLista: string = '';
  public pagination = {} as Pagination;

  public setorId = 0;

  // public get filtroLista(): string
  // {
  //     return this._filtroLista;
  // }

  // public set filtroLista(value: string)
  // {
  //     this._filtroLista = value;
  //     this.setoresFiltrados = this.filtroLista ? this.filtrarSetores(this.filtroLista) : this.setores;
  // }

  // public filtrarSetores(filtro: string): Setor[]
  // {
  //   filtro = filtro.toLocaleLowerCase();

  //   return this.setores.filter(setor => setor.nome.toLocaleLowerCase().indexOf(filtro) !== -1);
  // }

  termoBuscaChanged: Subject<string> = new Subject<string>();

  constructor(
    private setorService: SetorService,
    private modalService: BsModalService,
    private router: Router,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.pagination = { currentPage: 1, itemsPerPage: 5, totalItems: 1 } as Pagination;

    this.getSetores();

    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 5000);
  }

  public getSetores(): void {
    this.spinner.show();

    this.setorService.getPaginatedSetores(this.pagination.currentPage, this.pagination.itemsPerPage).subscribe({
      next: (paginatedResult: PaginatedResult<Setor[]>) =>{
        this.setores = paginatedResult.result;
        //this.setoresFiltrados = this.setores;
        this.pagination = paginatedResult.pagination;
      },
      error: () => {
        this.spinner.hide();
        this.toastr.error('Falha ao carregar setores','Falha');
      },
      complete: () => this.spinner.hide()
    });
  }

  public filtrarSetores(evento: any): void
  {
    if(this.termoBuscaChanged.observers.length === 0){
      this.termoBuscaChanged.pipe(debounceTime(1000)).subscribe(
        filtrarPor => {
          this.spinner.show();
          this.setorService.getPaginatedSetores(this.pagination.currentPage,
            this.pagination.itemsPerPage, filtrarPor).subscribe({
              next: (paginatedResult: PaginatedResult<Setor[]>) =>{
                this.setores = paginatedResult.result;
                this.pagination = paginatedResult.pagination;
              },
              error: () => {
                this.spinner.hide();
                this.toastr.error('Falha ao carregar setores','Falha');
              }
              //,complete: () => this.spinner.hide()
            }).add(() => this.spinner.hide());
        }
      );
    }

    this.termoBuscaChanged.next(evento.value);
  }

  confirm(): void {
    this.modalRef?.hide();
    this.spinner.show();

    this.setorService.delete(this.setorId).subscribe({
      next: (result: any) => {
        console.log(result);
        if(result.message === 'Deletado') {
          this.toastr.success('Setor deletado com sucesso!', 'Sucesso');
          this.spinner.hide();
          this.getSetores();
        }
      },
      error: (error: any) => {
        console.error(error);
        this.toastr.error('Falha ao Excluir Setor', 'Falha');
        this.spinner.hide();
      },
      complete: () => this.spinner.hide()
    });


  }

  decline(): void {
    this.modalRef?.hide();
  }

  //Caso o redirecionamento para edição esteja na table (bom pra celulares)
  //Se for usar colocar injeção de dependência para Router no constructor: private router: Router
  //detalheEvento(id: number): void {
  //  this.router.navigate([`setores/detalhe/${id}`]);
  //}

  detalheSetor(id: number): void {
    this.router.navigate([`setores/detalhe/${id}`]);
  }

  //Antigo
  //openModal(template: TemplateRef<any>): void {
  //  this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
  //}
  //Novo, para evitar Propagation qdo edição está em clique de linha
  openModal(event: any, template: TemplateRef<any>, setorId: number): void {
    event.stopPropagation();
    this.setorId = setorId;
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
  }

  public pageChanged(event): void {
    this.pagination.currentPage = event.page;
    this.getSetores();
  }
}
