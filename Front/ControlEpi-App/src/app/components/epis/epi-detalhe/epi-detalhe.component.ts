import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Epi } from 'src/app/models/Epi';
import { EpiService } from 'src/app/services/epi.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-epi-detalhe',
  templateUrl: './epi-detalhe.component.html',
  styleUrls: ['./epi-detalhe.component.scss']
})
export class EpiDetalheComponent implements OnInit {

  form!: FormGroup;
  epi = {} as Epi;
  epiId: number;
  imagemURL = 'assets/img/upload.png';
  file: File;
  tipoSalvamento = 'post';

  get modoEditar(): boolean {
    return this.tipoSalvamento === 'put';
  }

  get f(): any {
    return this.form.controls;
  }

  constructor(
    private fb: FormBuilder,
    private epiService: EpiService,
    private router: Router,
    private activatedRouter: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.carregarEpi();
    this.validation();
  }

  public carregarEpi(): void {
    this.epiId = +this.activatedRouter.snapshot.paramMap.get('id');

    if(this.epiId !== null  && this.epiId !== 0){
      this.spinner.show();

      this.tipoSalvamento = 'put';

      this.epiService.getEpiById(this.epiId).subscribe({
        next: (epi: Epi) => {
          this.epi = {...epi};
          this.form.patchValue(this.epi);

          if (this.epi.foto !== '') {
            this.imagemURL = environment.apiURL + 'resources/images/' + this.epi.foto;
          }
        },
        error: (error: any) => {
          this.spinner.hide();
          this.toastr.error('Falha ao carregar EPI', 'Falha');
          console.error(error);
        },
        complete: () => this.spinner.hide()
      });
    }
  }

  public salvarAlteracao(): void {
    this.spinner.show();

    if(this.form.valid) {
      this.epi =
        this.tipoSalvamento === 'post'
          ? { ...this.form.value }
          : { id: this.epi.id, ...this.form.value };

      this.epiService[this.tipoSalvamento](this.epi).subscribe(
        (epiRetorno: Epi) => {
          this.toastr.success('EPI salvo com Sucesso!', 'Sucesso');
          this.router.navigate([`epis/detalhe/${epiRetorno.id}`]);
        },
        (error: any) => {
          console.error(error);
          this.toastr.error('Error ao salvar epi', 'Erro');
        }
      ).add(() => this.spinner.hide());


    }
  }

  public validation(): void {
    this.form = this.fb.group({
      nome: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(200)]],
      codigoInterno: [''],
      marca: [''],
      prazoValidadeDias: [''],
      qtdeEstoque: [''],
      custo: [''],
      foto: ['']
    });
  }

  onSubmit(): void {
    if(this.form.invalid) {
      return;
    }
  }

  public resetForm(event: any): void {
    event.preventDefault();
    this.form.reset();
  }

  onFileChange(ev: any): void {
    const reader = new FileReader();

    reader.onload = (event: any) => this.imagemURL = event.target.result;

    this.file = ev.target.files;
    reader.readAsDataURL(this.file[0]);

    this.uploadImagem();
  }

  private uploadImagem(): void {
    this.spinner.show();
    this.epiService.postUpload(this.epiId, this.file).subscribe(
      () => {
        this.carregarEpi();
        this.toastr.success('Foto atualizada com Sucesso', 'Sucesso!');
      },
      (error: any) => {
        this.toastr.error('Erro ao fazer upload de foto', 'Erro!');
        console.log(error);
      }
    ).add(() => this.spinner.hide());
  }
}
