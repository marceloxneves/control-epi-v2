import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EpiDetalheComponent } from './epi-detalhe.component';

describe('EpiDetalheComponent', () => {
  let component: EpiDetalheComponent;
  let fixture: ComponentFixture<EpiDetalheComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EpiDetalheComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EpiDetalheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
