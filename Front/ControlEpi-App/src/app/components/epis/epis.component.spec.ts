import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EpisComponent } from './epis.component';

describe('EpisComponent', () => {
  let component: EpisComponent;
  let fixture: ComponentFixture<EpisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EpisComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EpisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
