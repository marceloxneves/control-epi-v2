import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { debounceTime, Subject } from 'rxjs';
import { Epi } from 'src/app/models/Epi';
import { PaginatedResult, Pagination } from 'src/app/models/Pagination';
import { EpiService } from 'src/app/services/epi.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-epi-lista',
  templateUrl: './epi-lista.component.html',
  styleUrls: ['./epi-lista.component.scss']
})
export class EpiListaComponent implements OnInit {

  modalRef?: BsModalRef;
  public largFoto: number = 70;
  public margFoto: number = 2;

  public epis: Epi[] = [];
  //public episFiltrados: Epi[] = [];
  //private _filtroLista: string = '';
  public pagination = {} as Pagination;

  public epiId = 0;

  // public get filtroLista(): string
  // {
  //     return this._filtroLista;
  // }

  // public set filtroLista(value: string)
  // {
  //   this._filtroLista = value;
  //   this.episFiltrados = this.filtroLista ? this.filtrarEpis(this.filtroLista) : this.epis;
  // }

  // public filtrarEpis(filtro: string): Epi[]
  // {
  //   filtro = filtro.toLocaleLowerCase();

  //   return this.epis.filter(epi => epi.nome.toLocaleLowerCase().indexOf(filtro) !== -1);
  // }
  // public filtrarEpis(evento: any): void
  // {
  //   this.epiService.getPaginatedEpis(this.pagination.currentPage,
  //     this.pagination.itemsPerPage, evento.value).subscribe({
  //       next: (paginatedResult: PaginatedResult<Epi[]>) =>{
  //         this.epis = paginatedResult.result;
  //         this.pagination = paginatedResult.pagination;
  //       },
  //       error: () => {
  //         this.spinner.hide();
  //         this.toastr.error('Falha ao carregar epi(s)','Falha');
  //       },
  //       complete: () => this.spinner.hide()
  //     });
  // }

  termoBuscaChanged: Subject<string> = new Subject<string>();

  constructor(private epiService: EpiService,
    private modalService: BsModalService,
    private router: Router,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.pagination = { currentPage: 1, itemsPerPage: 5, totalItems: 1 } as Pagination;

    this.getEpis();

    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 5000);
  }

  public getEpis(): void {
    this.spinner.show();

    this.epiService.getPaginatedEpis(this.pagination.currentPage, this.pagination.itemsPerPage).subscribe({
      next: (paginatedResult: PaginatedResult<Epi[]>) =>{
        this.epis = paginatedResult.result;
        //this.episFiltrados = this.epis;
        this.pagination = paginatedResult.pagination;
      },
      error: () => {
        this.spinner.hide();
        this.toastr.error('Falha ao carregar epis','Falha');
      }
      //, complete: () => this.spinner.hide()
    }).add(() => this.spinner.hide());
  }

  public filtrarEpis(evento: any): void
  {
    if(this.termoBuscaChanged.observers.length === 0){
      this.termoBuscaChanged.pipe(debounceTime(1000)).subscribe(
        filtrarPor => {
          this.spinner.show();
          this.epiService.getPaginatedEpis(this.pagination.currentPage,
            this.pagination.itemsPerPage, filtrarPor).subscribe({
              next: (paginatedResult: PaginatedResult<Epi[]>) =>{
                this.epis = paginatedResult.result;
                this.pagination = paginatedResult.pagination;
              },
              error: () => {
                this.spinner.hide();
                this.toastr.error('Falha ao carregar EPI\'s','Falha');
              }
              //,complete: () => this.spinner.hide()
            }).add(() => this.spinner.hide());
        }
      );
    }

    this.termoBuscaChanged.next(evento.value);
  }

  detalheEpi(id: number): void {
    this.router.navigate([`epis/detalhe/${id}`]);
  }

  public exibirFoto(fotoURL: string): string {
    return fotoURL !== ''
      ? `${environment.apiURL}resources/images/${fotoURL}`
      : 'assets/img/semImagem.jpeg';
  }

  confirm(): void {
    this.modalRef?.hide();
    this.spinner.show();

    this.epiService.delete(this.epiId).subscribe({
      next: (result: any) => {
        console.log(result);
        if(result.message === 'Deletado') {
          this.toastr.success('EPI deletado com sucesso!', 'Sucesso');
          this.spinner.hide();
          this.getEpis();
        }
      },
      error: (error: any) => {
        console.error(error);
        this.toastr.error('Falha ao Excluir EPI', 'Falha');
        this.spinner.hide();
      },
      complete: () => this.spinner.hide()
    });
  }

  decline(): void {
    this.modalRef?.hide();
  }

  //Caso o redirecionamento para edição esteja na table (bom pra celulares)
  //Se for usar colocar injeção de dependência para Router no constructor: private router: Router
  //detalheEvento(id: number): void {
  //  this.router.navigate([`setores/detalhe/${id}`]);
  //}

  //Antigo
  //openModal(template: TemplateRef<any>): void {
  //  this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
  //}
  //Novo, para evitar Propagation qdo edição está em clique de linha
  openModal(event: any, template: TemplateRef<any>, epiId: number): void {
    event.stopPropagation();
    this.epiId = epiId;
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
  }

  public pageChanged(event): void {
    this.pagination.currentPage = event.page;
    this.getEpis();
  }
}
