import { Component, OnInit } from '@angular/core';
import { AbstractControlOptions, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinner, NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ValidatorField } from 'src/app/helpers/ValidatorField';
import { UserUpdate } from 'src/app/models/identity/UserUpdate';
import { AccountService } from 'src/app/services/account.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.scss']
})
export class PerfilComponent implements OnInit {
  public perfilImgUrl = '';
  public file: File;
  public userUpdate = {} as UserUpdate;
  form!: FormGroup;

  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    private accountService: AccountService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    this.validation();
    this.carregarUsuario();
  }

  private validation(): void {
    const formOptions: AbstractControlOptions = {
      validators: ValidatorField.MustMatch('password', 'confirmePassword'),
    };

    this.form = this.fb.group(
      {
        userName: [''],
        perfilImgUrl: [''],
        nome: ['', Validators.required],
        email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.minLength(4), Validators.nullValidator]],
        confirmePassword: ['', Validators.nullValidator],
      },
      formOptions
    );
  }

  // public setFormValue(usuario: UserUpdate): void {
  //   this.userUpdate = usuario;
  //   if (this.userUpdate.imagemURL)
  //     this.imagemURL = environment.apiURL + `resources/perfil/${this.userUpdate.imagemURL}`;
  //   else
  //     this.imagemURL = './assets/img/perfil.png';

  // }

  private carregarUsuario(): void {
    this.spinner.show();

    this.accountService.getUser().subscribe({
      next: (userRetorno: UserUpdate) => {
        console.log(userRetorno);
        this.userUpdate = userRetorno;

        if (this.userUpdate.perfilImgUrl)
          this.perfilImgUrl = environment.apiURL + `resources/perfil/${this.userUpdate.perfilImgUrl}`;
        else
          this.perfilImgUrl = './assets/img/perfil.png';

        this.form.patchValue(this.userUpdate);
        this.toastr.success('Usuário Carregado','Sucesso');
      },
      error: (err) => {
        console.log(err);
        this.toastr.error('Usuário não Carregado', 'Erro');
        this.router.navigate(['/home']);
      }
    }).add(() => this.spinner.hide());
  }

  // Conveniente para pegar um FormField apenas com a letra F
  get f(): any {
    return this.form.controls;
  }

  onSubmit(): void {
    this.atualizarUsuario();
  }

  public atualizarUsuario() {
    this.userUpdate = { ...this.form.value };
    this.spinner.show();

    this.accountService
      .updateUser(this.userUpdate)
      .subscribe({
        next: () => this.toastr.success('Usuário atualizado!', 'Sucesso'),
        error: (error) => {
          this.toastr.error(error.error);
          console.error(error);
        }
      })
      .add(() => this.spinner.hide());
  }

  onFileChange(ev: any): void {
    const reader = new FileReader();

    reader.onload = (event: any) => this.perfilImgUrl = event.target.result;

    this.file = ev.target.files;
    reader.readAsDataURL(this.file[0]);

    this.uploadImagem();
  }

  private uploadImagem(): void {
    this.spinner.show();
    this.accountService.postUpload(this.file).subscribe({
      next: () => {
        //this.carregarEpi();
        this.toastr.success('Foto atualizada com Sucesso', 'Sucesso!');
      },
      error: (error: any) => {
        this.toastr.error('Erro ao fazer upload de foto', 'Erro!');
        console.log(error);
      }
    }).add(() => this.spinner.hide());
  }

  public resetForm(event: any): void {
    event.preventDefault();
    this.form.reset();
  }
}
