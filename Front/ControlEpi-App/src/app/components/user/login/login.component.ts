import { compileNgModule } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { User } from 'src/app/models/identity/User';
import { UserLogin } from 'src/app/models/identity/UserLogin';
import { AccountService } from 'src/app/services/account.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  model = {} as UserLogin;

  constructor(private accountService: AccountService, private router: Router, private toaster: ToastrService) { }

  ngOnInit(): void {
    //para não sumir o menu superior
    //https://stackoverflow.com/questions/61587593/how-to-prevent-continuously-refreshing-the-page-in-angular-using-window-location
    if (!localStorage.getItem('firstReload') || localStorage.getItem('firstReload') == 'true') {
      localStorage.setItem('firstReload', 'false');
      window.location.reload();
    } else {
      localStorage.setItem('firstReload', 'true');
    }
  }

  public login(): void {
    this.accountService.login(this.model).subscribe({
      next: () => {
        this.router.navigate(['/home']);
        //this.router.navigateByUrl(`/home`);
        //this.router.navigateByUrl('home');
      },
      error: (error: any) => {
        //if(error.status == 401)
        //{
          this.toaster.error('Usuário ou Senha inválido(s)');
        //}
      }
    });
  }
}
