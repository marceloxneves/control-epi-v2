import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Empresa } from 'src/app/models/Empresa';
import { EmpresaService } from 'src/app/services/empresa.service';

@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.component.html',
  styleUrls: ['./empresa.component.scss']
})
export class EmpresaComponent implements OnInit {

  form!: FormGroup;

  empresa = {} as Empresa;

  //empresaId: number;

  get f(): any {
    return this.form.controls;
  }

  constructor(
    private fb: FormBuilder,
    private empresaService: EmpresaService,
    private router: Router,
    private activatedRouter: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.carregarEmpresa();
    this.validation();
  }

  public carregarEmpresa(): void {
      this.spinner.show();

      this.empresaService.getUnica().subscribe({
        next: (empresa: Empresa) => {
          //this.empresaId = empresa.id;
          this.empresa = {...empresa};
          this.form.patchValue(this.empresa);
        },
        error: (error: any) => {
          this.spinner.hide();
          this.toastr.error('Falha ao carregar Empresa', 'Falha');
          console.error(error);
        },
        complete: () => this.spinner.hide()
      });
  }

  public salvarAlteracao(): void {
    this.spinner.show();

    if(this.form.valid) {
      this.empresa = { id: this.empresa.id, ...this.form.value };

      this.empresaService['put'](this.empresa).subscribe(
        (empresaRetorno: Empresa) => {
          this.toastr.success('Empresa salva com Sucesso!', 'Sucesso');
          this.router.navigate([`user/empresa`]);
        },
        (error: any) => {
          console.error(error);
          this.toastr.error('Error ao salvar Empresa', 'Erro');
        }
      ).add(() => this.spinner.hide());
    }
  }

  public validation(): void {
    this.form = this.fb.group({
      razaoSocial: [''],
      cnpj: [''],
      inscrEst: [''],
      endereco: [''],
      municipio: [''],
      uf: [''],
      cep: [''],
      telefone: [''],
      email: [''],
    });
  }

  onSubmit(): void {
    if(this.form.invalid) {
      return;
    }
  }

  public resetForm(event: any): void {
    event.preventDefault();
    this.form.reset();
  }

}
