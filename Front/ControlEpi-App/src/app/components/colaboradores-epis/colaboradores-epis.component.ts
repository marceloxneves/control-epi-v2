import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Colaborador } from 'src/app/models/Colaborador';
import { Epi } from 'src/app/models/Epi';
import { ColaboradorEpiService } from 'src/app/services/colaborador-epi.service';

@Component({
  selector: 'app-colaboradores-epis',
  templateUrl: './colaboradores-epis.component.html',
  styleUrls: ['./colaboradores-epis.component.scss']
})
export class ColaboradoresEpisComponent implements OnInit {

  epis: Epi[];
  colaboradores: Colaborador[];

  constructor(
    private colaboradorEpiService: ColaboradorEpiService,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.carregarEpis();
    this.carregarColaboradores();
  }

  public carregarEpis(): void {
    this.colaboradorEpiService.getEpis().subscribe({
      next: (epis: Epi[]) => {
        this.epis = epis;
        //this.form.patchValue(this.colaborador);
      },
      error: (error: any) => {
        this.toastr.error('Falha ao carregar EPIs', 'Falha');
        console.error(error);
      }
    });
  }

  public carregarColaboradores(): void {
    this.colaboradorEpiService.getColaboradores().subscribe({
      next: (colaboradores: Colaborador[]) => {
        this.colaboradores = colaboradores;
        //this.form.patchValue(this.colaborador);
      },
      error: (error: any) => {
        this.toastr.error('Falha ao carregar Colaboradores', 'Falha');
        console.error(error);
      }
    });
  }

}
