import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ColaboradoresEpisComponent } from './colaboradores-epis.component';

describe('ColaboradoresEpisComponent', () => {
  let component: ColaboradoresEpisComponent;
  let fixture: ComponentFixture<ColaboradoresEpisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ColaboradoresEpisComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ColaboradoresEpisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
