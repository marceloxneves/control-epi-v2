import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Colaborador } from '../models/Colaborador';
import { ColaboradorEpi } from '../models/ColaboradorEpi';
import { Epi } from '../models/Epi';
import { ColaboradorService } from './colaborador.service';
import { EpiService } from './epi.service';

@Injectable({
  providedIn: 'root'
})
export class ColaboradorEpiService {
  baseUrlColaboradorEpi = environment.apiURL + 'api/ColaboradorEpi';

  constructor(
    private http: HttpClient,
    private colaboradorService: ColaboradorService,
    private epiService: EpiService) { }

  public getColaboradoresEpisByColaborador(colaboradorId: number): Observable<ColaboradorEpi[]> {
    return this.http.get<ColaboradorEpi[]>(`${this.baseUrlColaboradorEpi}/${colaboradorId}`).pipe(take(1));
  }

  public post(colaboradorId: number, epiId: number, colaboradorEpi: ColaboradorEpi[]): Observable<ColaboradorEpi> {
    return this.http
      .post<ColaboradorEpi>(`${this.baseUrlColaboradorEpi}/${colaboradorId}/${epiId}`, colaboradorEpi).pipe(take(1));
  }

  public devolver(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrlColaboradorEpi}/${id}/${true}`).pipe(take(1));
  }

  public getColaboradores(): Observable<Colaborador[]>
  {
    return this.colaboradorService.getColaboradores();
  }

  public getEpis(): Observable<Epi[]> {
    return this.epiService.getEpis();
  }
}
