import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Epi } from '../models/Epi';
import { PaginatedResult } from '../models/Pagination';

@Injectable({
  providedIn: 'root'
})
export class EpiService {
  baseUrlEpis = environment.apiURL + 'api/epis';

  constructor(private http: HttpClient) { }

  public getEpis(): Observable<Epi[]> {
    return this.http.get<Epi[]>(this.baseUrlEpis);
  }

  public getPaginatedEpis(page?: number, itemsPerPage?: number, term?: string): Observable<PaginatedResult<Epi[]>> {
    const paginatedResult: PaginatedResult<Epi[]> = new PaginatedResult<Epi[]>();

    let params = new HttpParams;

    if (page != null && itemsPerPage != null) {
      params = params.append('pageNumber', page.toString());
      params = params.append('pageSize', itemsPerPage.toString());
    }

    if (term != null && term != '')
      params = params.append('term', term)

    return this.http
      .get<Epi[]>(this.baseUrlEpis, {observe: 'response', params })
      .pipe(
        take(1),
        map((response) => {
          paginatedResult.result = response.body;
          if(response.headers.has('Pagination')) {
            paginatedResult.pagination = JSON.parse(response.headers.get('Pagination'));
          }
          return paginatedResult;
        }));
  }

  public getEpiById(id: number): Observable<Epi> {
    return this.http.get<Epi>(`${ this.baseUrlEpis }/${ id }`);
  }

  public getEpisByName(name: string): Observable<Epi[]> {
    return this.http.get<Epi[]>(`${ this.baseUrlEpis }/${ name }`);
  }

  public post(epi: Epi): Observable<Epi> {
    return this.http
      .post<Epi>(this.baseUrlEpis, epi).pipe(take(1));
  }

  public put(epi: Epi): Observable<Epi> {
    return this.http
      .put<Epi>(`${this.baseUrlEpis}/${epi.id}`, epi).pipe(take(1));
  }

  public delete(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrlEpis}/${id}`).pipe(take(1));
  }

  postUpload(epiId: number, file: File): Observable<Epi> {
    const fileToUpload = file[0] as File;
    const formData = new FormData();
    formData.append('file', fileToUpload);

    return this.http
      .post<Epi>(`${this.baseUrlEpis}/upload-foto-epi/${epiId}`, formData)
      .pipe(take(1));
  }
}
