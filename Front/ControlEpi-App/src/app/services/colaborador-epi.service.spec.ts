import { TestBed } from '@angular/core/testing';

import { ColaboradorEpiService } from './colaborador-epi.service';

describe('ColaboradorEpiService', () => {
  let service: ColaboradorEpiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ColaboradorEpiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
