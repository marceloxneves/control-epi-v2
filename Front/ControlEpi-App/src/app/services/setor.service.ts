import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { PaginatedResult } from '../models/Pagination';
import { Setor } from '../models/Setor';

@Injectable({
  providedIn: 'root'
})
export class SetorService {
  baseUrlSetores = environment.apiURL + 'api/setores';

  //tokenHeader = new HttpHeaders({'Authorization':'Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJuYW1laWQiOiIzIiwidW5pcXVlX25hbWUiOiJtYXJjZWxveG5ldmVzIiwibmJmIjoxNjY0MzE4Mzk2LCJleHAiOjE2NjQ0MDQ3OTYsImlhdCI6MTY2NDMxODM5Nn0.iBDpJviFUxfITeqOFycAgg-KBC_k6zUMh8knLy9aSO7FAro-Rgcen9E2he7SZoDl6vqYPwTqQBe6daMCDtAlzw'});

  constructor(private http: HttpClient) { }

  public getSetores(): Observable<Setor[]> {
    return this.http.get<Setor[]>(this.baseUrlSetores).pipe(take(1));
  }

  public getPaginatedSetores(page?: number, itemsPerPage?: number, term?: string): Observable<PaginatedResult<Setor[]>> {
    const paginatedResult: PaginatedResult<Setor[]> = new PaginatedResult<Setor[]>();

    let params = new HttpParams;

    if (page != null && itemsPerPage != null) {
      params = params.append('pageNumber', page.toString());
      params = params.append('pageSize', itemsPerPage.toString());
    }

    if (term != null && term != '')
      params = params.append('term', term)

    return this.http
      .get<Setor[]>(this.baseUrlSetores, {observe: 'response', params })
      .pipe(
        take(1),
        map((response) => {
          paginatedResult.result = response.body;
          if(response.headers.has('Pagination')) {
            paginatedResult.pagination = JSON.parse(response.headers.get('Pagination'));
          }
          return paginatedResult;
        }));
  }

  public getSetorById(id: number): Observable<Setor> {
    return this.http.get<Setor>(`${ this.baseUrlSetores }/${ id }`).pipe(take(1));
  }

  // public getSetoresByName(name: string): Observable<Setor[]> {
  //   return this.http.get<Setor[]>(`${ this.baseUrlSetores }/${ name }`).pipe(take(1));
  // }

  public post(setor: Setor): Observable<Setor> {
    return this.http
      .post<Setor>(this.baseUrlSetores, setor).pipe(take(1));
  }

  public put(setor: Setor): Observable<Setor> {
    return this.http
      .put<Setor>(`${this.baseUrlSetores}/${setor.id}`, setor).pipe(take(1));
  }

  public delete(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrlSetores}/${id}`).pipe(take(1));
  }
}
