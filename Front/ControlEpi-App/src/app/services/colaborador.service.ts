import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Colaborador } from '../models/Colaborador';
import { map, take } from 'rxjs/operators';
import { Setor } from '../models/Setor';
import { SetorService } from './setor.service';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { PaginatedResult } from '../models/Pagination';

@Injectable({
  providedIn: 'root'
})
export class ColaboradorService {
  baseUrlColaboradores = environment.apiURL + 'api/colaboradores';
  //passado para interceptor
  //tokenHeader = new HttpHeaders({ 'Authorization': `Bearer ${JSON.parse(localStorage.getItem('user')).token}` });

  constructor(private http: HttpClient, private setorService: SetorService) { }

  public getColaboradores(): Observable<Colaborador[]>
  {
    //passado para interceptor
    //return this.http.get<Colaborador[]>(this.baseUrlColaboradores, { headers: this.tokenHeader }).pipe(take(1));
    return this.http.get<Colaborador[]>(this.baseUrlColaboradores).pipe(take(1));
  }

  public getPaginatedColaboradores(page?: number, itemsPerPage?: number, term?: string): Observable<PaginatedResult<Colaborador[]>> {
    const paginatedResult: PaginatedResult<Colaborador[]> = new PaginatedResult<Colaborador[]>();

    let params = new HttpParams;

    if (page != null && itemsPerPage != null) {
      params = params.append('pageNumber', page.toString());
      params = params.append('pageSize', itemsPerPage.toString());
    }

    if (term != null && term != '')
      params = params.append('term', term)

    return this.http
      .get<Colaborador[]>(this.baseUrlColaboradores, {observe: 'response', params })
      .pipe(
        take(1),
        map((response) => {
          paginatedResult.result = response.body;
          if(response.headers.has('Pagination')) {
            paginatedResult.pagination = JSON.parse(response.headers.get('Pagination'));
          }
          return paginatedResult;
        }));
  }

  public getColaboradorById(id: number): Observable<Colaborador>
  {
    return this.http.get<Colaborador>(`${ this.baseUrlColaboradores }/${id}`).pipe(take(1));
  }

  public getColaboradoresByName(name: string): Observable<Colaborador[]>
  {
    return this.http.get<Colaborador[]>(`${ this.baseUrlColaboradores }/${ name }`).pipe(take(1));
  }

  public getSetores(): Observable<Setor[]> {
    return this.setorService.getSetores();
  }

  public post(colaborador: Colaborador): Observable<Colaborador> {
    return this.http
      .post<Colaborador>(this.baseUrlColaboradores, colaborador).pipe(take(1));
  }

  public put(colaborador: Colaborador): Observable<Colaborador> {
    return this.http
      .put<Colaborador>(`${this.baseUrlColaboradores}/${colaborador.id}`, colaborador).pipe(take(1));
  }

  public delete(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrlColaboradores}/${id}`).pipe(take(1));
  }
}
