import { Component, HostListener, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { map, Observable, Subscription } from 'rxjs';
import { User } from 'src/app/models/identity/User';
import { AccountService } from 'src/app/services/account.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {
  isCollapsed = true;
  private subs: Subscription[] = [];

  constructor(
    //private host: ElementRef<HTMLElement>, para destruir componente
    public accountService: AccountService,
    private router: Router) { }

  ngOnInit(): void {
  }

  // @HostListener('unloaded')
  // ngOnDestroy() {
  //   // unsubscribe from all on destroy
  //   this.subs.forEach(sub => sub.unsubscribe());
  // }

  logout(): void{
    this.accountService.logout();
    this.router.navigateByUrl('/user/login');
    // this.host.nativeElement.remove(); para destruir componente
  }

  showMenu(): boolean {
    return this.router.url !== '/user/login' && this.router.url !== '/user/registration';
  }
}
