﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ControlEpi.Repository.Migrations
{
    public partial class InclusaoEpiDevolvido : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Devolvido",
                table: "ColaboradoresEpis",
                type: "INTEGER",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Devolvido",
                table: "ColaboradoresEpis");
        }
    }
}
