﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ControlEpi.Repository.Migrations
{
    public partial class Second : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ColaboradoresEpis",
                table: "ColaboradoresEpis");

            migrationBuilder.AlterColumn<int>(
                name: "Id",
                table: "ColaboradoresEpis",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ColaboradoresEpis",
                table: "ColaboradoresEpis",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_ColaboradoresEpis_ColaboradorId",
                table: "ColaboradoresEpis",
                column: "ColaboradorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ColaboradoresEpis",
                table: "ColaboradoresEpis");

            migrationBuilder.DropIndex(
                name: "IX_ColaboradoresEpis_ColaboradorId",
                table: "ColaboradoresEpis");

            migrationBuilder.AlterColumn<int>(
                name: "Id",
                table: "ColaboradoresEpis",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ColaboradoresEpis",
                table: "ColaboradoresEpis",
                columns: new[] { "ColaboradorId", "EpiId" });
        }
    }
}
