﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ControlEpi.Repository.Migrations
{
    public partial class retiradausercolaborador : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Colaboradores_AspNetUsers_UserId",
                table: "Colaboradores");

            migrationBuilder.DropIndex(
                name: "IX_Colaboradores_UserId",
                table: "Colaboradores");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Colaboradores");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Colaboradores",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Colaboradores_UserId",
                table: "Colaboradores",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Colaboradores_AspNetUsers_UserId",
                table: "Colaboradores",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
