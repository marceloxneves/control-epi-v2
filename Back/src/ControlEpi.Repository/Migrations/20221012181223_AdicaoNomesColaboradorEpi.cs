﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ControlEpi.Repository.Migrations
{
    public partial class AdicaoNomesColaboradorEpi : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ColaboradorNome",
                table: "ColaboradoresEpis",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EpiNome",
                table: "ColaboradoresEpis",
                type: "TEXT",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ColaboradorNome",
                table: "ColaboradoresEpis");

            migrationBuilder.DropColumn(
                name: "EpiNome",
                table: "ColaboradoresEpis");
        }
    }
}
