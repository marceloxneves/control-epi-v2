using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Domain.Entities;
using ControlEpi.Domain.Entities.PageList;
using ControlEpi.Domain.Interfaces;
using ControlEpi.Repository.Context;
using Microsoft.EntityFrameworkCore;

namespace ControlEpi.Repository.Implementations
{
    public class EpiRepository : BaseRepository<Epi>, IEpiRepository
    {
        public EpiRepository(ControlEpiContext context) : base(context)
        {}

        public async Task<PageList<Epi>> GetAllAsync(PageParams pageParams)
        {
            IQueryable<Epi> epis = DbSet
                .Include(e => e.ColaboradoresEpis);
                //.ThenInclude(ce => ce.Epi);

            if(!string.IsNullOrWhiteSpace(pageParams.Term))
            {
                epis = epis.Where(e => e.Nome.ToLower().Contains(pageParams.Term));
            }

            return await PageList<Epi>.CreateAsync(epis, pageParams.PageNumber, pageParams.PageSize);
        }

        public override async Task<Epi> GetByIdAsync(int id)
        {
            IQueryable<Epi> epis = DbSet.Where(e => e.Id == id)
                .Include(e => e.ColaboradoresEpis);
                //.ThenInclude(ce => ce.Epi);

            return await epis.FirstOrDefaultAsync();
        }
    }
}