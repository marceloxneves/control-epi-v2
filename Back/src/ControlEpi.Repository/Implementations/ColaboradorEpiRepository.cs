using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Domain.Entities;
using ControlEpi.Domain.Interfaces;
using ControlEpi.Repository.Context;
using Microsoft.EntityFrameworkCore;

namespace ControlEpi.Repository.Implementations
{
    public class ColaboradorEpiRepository : BaseRepository<ColaboradorEpi>, IColaboradorEpiRepository
    {

        public ColaboradorEpiRepository(ControlEpiContext context) : base(context)
        {}

        public async Task<ColaboradorEpi[]> GetColaboradorEpiByColaboradorIdAsync(int colaboradorId)
        {
            IQueryable<ColaboradorEpi> query =  DbSet.Where(ce => ce.ColaboradorId == colaboradorId && !ce.Devolvido);

            return await query.ToArrayAsync();
        }

        public override async Task<ColaboradorEpi> GetByIdAsync(int id)
        {
            return await DbSet.FindAsync(id);
        }
    }
}