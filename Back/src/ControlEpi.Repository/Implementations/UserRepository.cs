using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Domain.Interfaces;
using ControlEpi.Domain.Identity;
using ControlEpi.Repository.Context;
using Microsoft.EntityFrameworkCore;

namespace ControlEpi.Repository.Implementations
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        private readonly ControlEpiContext context;

        public UserRepository(ControlEpiContext context) : base(context)
        {
            this.context = context;
            
        }

        public async Task<User> GetUserByIdAsync(int id)
        {
            return await context.Users.FindAsync(id);
        }

        public async Task<User> GetUserByUsernameAsync(string username)
        {
            return await context.Users.SingleOrDefaultAsync(u => u.UserName.ToLower() == username.ToLower());
        }

        public async Task<IEnumerable<User>> GetUsersAsync()
        {
            return await context.Users.ToListAsync();
        }
    }
}