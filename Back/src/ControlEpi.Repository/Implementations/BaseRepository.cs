using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Domain.Interfaces;
using ControlEpi.Repository.Context;
using Microsoft.EntityFrameworkCore;

namespace ControlEpi.Repository.Implementations
{
    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        //private readonly ControlEpiContext _contexto;
        protected ControlEpiContext Db;
        protected DbSet<T> DbSet;

        public BaseRepository(ControlEpiContext contexto)
        {
            Db = contexto;

            //Consultas passam a usar NoTracking() 
            //Db.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

            DbSet = Db.Set<T>();
            
        }

        public void Add(T entity)
        {
            DbSet.Add(entity);
        }

        public void Delete(T entity)
        {
            DbSet.Remove(entity);
        }

        public void DeleteRange(IEnumerable<T> entities)
        {
            DbSet.RemoveRange(entities);
        }

        public virtual async Task<T> GetByIdAsync(int id)
        {
            return await DbSet.FindAsync(id);
        }

        public async Task<bool> SaveChangesAsync()
        {
            return (await Db.SaveChangesAsync()) > 0;
        }

        public void Update(T entity)
        {
            DbSet.Update(entity);
        }
    }
}