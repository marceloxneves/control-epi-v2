using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Domain.Entities;
using ControlEpi.Domain.Entities.PageList;
using ControlEpi.Domain.Interfaces;
using ControlEpi.Repository.Context;
using Microsoft.EntityFrameworkCore;

namespace ControlEpi.Repository.Implementations
{
    public class EmpresaRepository : BaseRepository<Empresa>, IEmpresaRepository
    {
        public EmpresaRepository(ControlEpiContext context) : base(context)
        {}

        public async Task<PageList<Empresa>> GetAllAsync(PageParams pageParams)
        {
            IQueryable<Empresa> empresas = DbSet;

            if(!string.IsNullOrWhiteSpace(pageParams.Term))
            {
                empresas = empresas.Where(s => s.RazaoSocial.ToLower().Contains(pageParams.Term));
            }

            return await PageList<Empresa>.CreateAsync(empresas, pageParams.PageNumber, pageParams.PageSize);
        }

        public override async Task<Empresa> GetByIdAsync(int id)
        {
            return await DbSet.FindAsync(id);
        }

        public async Task<Empresa> GetUnicaAsync()
        {
            return await DbSet.FirstOrDefaultAsync();
        }
    }
}