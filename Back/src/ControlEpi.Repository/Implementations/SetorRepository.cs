using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Domain.Entities;
using ControlEpi.Domain.Entities.PageList;
using ControlEpi.Domain.Interfaces;
using ControlEpi.Repository.Context;
using Microsoft.EntityFrameworkCore;

namespace ControlEpi.Repository.Implementations
{
    public class SetorRepository : BaseRepository<Setor>, ISetorRepository
    {
        public SetorRepository(ControlEpiContext context) : base(context)
        {}

        public async Task<PageList<Setor>> GetAllAsync(PageParams pageParams)
        {
            IQueryable<Setor> setores = DbSet;

            if(!string.IsNullOrWhiteSpace(pageParams.Term))
            {
                setores = setores.Where(s => s.Nome.ToLower().Contains(pageParams.Term.ToLower()));
            }

            return await PageList<Setor>.CreateAsync(setores, pageParams.PageNumber, pageParams.PageSize);
        }

        public override async Task<Setor> GetByIdAsync(int id)
        {
            return await DbSet.FindAsync(id);
        }
    }
}