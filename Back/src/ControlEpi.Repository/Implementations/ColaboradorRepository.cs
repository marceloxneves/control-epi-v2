using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Domain.Entities;
using ControlEpi.Domain.Entities.PageList;
using ControlEpi.Domain.Interfaces;
using ControlEpi.Repository.Context;
using Microsoft.EntityFrameworkCore;

namespace ControlEpi.Repository.Implementations
{
    public class ColaboradorRepository : BaseRepository<Colaborador>, IColaboradorRepository
    {
        public ColaboradorRepository(ControlEpiContext context) : base(context)
        {
        }

        public async Task<PageList<Colaborador>> GetAllAsync(PageParams pageParams)
        {
            IQueryable<Colaborador> colaboradores = DbSet
                .Include(c => c.Setor)
                .Include(c => c.ColaboradoresEpis);
                //.ThenInclude(ce => ce.Colaborador);

                if(!string.IsNullOrWhiteSpace(pageParams.Term))
                {
                    colaboradores = colaboradores.Where(c => c.Nome.ToLower().Contains(pageParams.Term.ToLower()));
                }

                return await PageList<Colaborador>.CreateAsync(colaboradores, pageParams.PageNumber, pageParams.PageSize);
        }

        public override async Task<Colaborador> GetByIdAsync(int id)
        {
            IQueryable<Colaborador> colaboradores = DbSet
                .Where(c => c.Id == id);

            return await colaboradores.FirstOrDefaultAsync();
        }

        public async Task<Colaborador> GetByIdEagerAsync(int id)
        {
            IQueryable<Colaborador> colaboradores = DbSet
                .Include(c => c.Setor)
                .Include(c => c.ColaboradoresEpis)
                //.ThenInclude(ce => ce.Colaborador)
                .Where(c => c.Id == id);

            return await colaboradores.FirstOrDefaultAsync();
        }
    }
}