using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Domain.Entities;
using ControlEpi.Domain.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ControlEpi.Repository.Context
{
    public class ControlEpiContext : IdentityDbContext<User, Role, int, IdentityUserClaim<int>, UserRole, IdentityUserLogin<int>, IdentityRoleClaim<int>, IdentityUserToken<int>>
    {
        public ControlEpiContext(DbContextOptions<ControlEpiContext> options) : base(options)
        { }

        public DbSet<Colaborador> Colaboradores { get; set; }
        public DbSet<ColaboradorEpi> ColaboradoresEpis { get; set; }
        public DbSet<Empresa> Empresas { get; set; }
        public DbSet<Epi> Epis { get; set; }
        public DbSet<Setor> Setores { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<UserRole>(userRole => {
                userRole.HasKey(ur => new { ur.UserId, ur.RoleId });

                userRole.HasOne(ur => ur.Role).WithMany(r => r.UserRoles).HasForeignKey(r => r.RoleId).IsRequired();
                userRole.HasOne(ur => ur.User).WithMany(r => r.UserRoles).HasForeignKey(r => r.UserId).IsRequired();
            });
            //modelBuilder.Entity<ColaboradorEpi>().HasKey(ce => new { ce.ColaboradorId, ce.EpiId });
            //modelBuilder.Entity<ColaboradorEpi>().HasKey(ce => ce.Id);

            //modelBuilder.Entity<ColaboradorEpi>()
            //    .Property(e => e.Id)
            //    .ValueGeneratedOnAdd();

            //Para cascade delete
            //modelBuilder.Entity<Colaborador>()
            //    .HasMany(c => c.ColaboradoresEpis)
            //    .WithOne(ce => ce.Colaborador)
            //    .OnDelete(DeleteBehavior.Cascade);
        }
        
    }
}