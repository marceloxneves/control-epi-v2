using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Domain.Entities;
using ControlEpi.Domain.Entities.PageList;

namespace ControlEpi.Domain.Interfaces
{
    public interface IEmpresaRepository : IBaseRepository<Empresa>
    {
        Task<PageList<Empresa>> GetAllAsync(PageParams pageParams);
        Task<Empresa> GetUnicaAsync();
    }
}