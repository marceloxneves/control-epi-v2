using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Domain.Entities;
using ControlEpi.Domain.Entities.PageList;

namespace ControlEpi.Domain.Interfaces
{
    public interface IColaboradorRepository : IBaseRepository<Colaborador>
    {
        Task<PageList<Colaborador>> GetAllAsync(PageParams pageParams);
        Task<Colaborador> GetByIdEagerAsync(int id);
    }
}