using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Domain.Entities;
using ControlEpi.Domain.Entities.PageList;

namespace ControlEpi.Domain.Interfaces
{
    public interface IEpiRepository : IBaseRepository<Epi>
    {
        Task<PageList<Epi>> GetAllAsync(PageParams pageParams);
    }
}