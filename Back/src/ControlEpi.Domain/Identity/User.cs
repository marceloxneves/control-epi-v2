using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace ControlEpi.Domain.Identity
{
    public class User : IdentityUser<int>
    {
        public string Nome { get; set; }
        public string perfilImgUrl { get; set; }
        public IEnumerable<UserRole> UserRoles { get; set; }
    }
}