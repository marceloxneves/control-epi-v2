using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Domain.Identity;

namespace ControlEpi.Domain.Entities
{
    public class Colaborador
    {
        public int Id { get; set; }
        // public int UserId { get; set; }
        // public User User { get; set; }
        public int SetorId { get; set; }
        public Setor Setor { get; set;}
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public string Rg { get; set; }
        public string Endereco { get; set; }
        public string Municipio { get; set; }
        public string Uf { get; set; }
        public string Cep { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }
        public IEnumerable<ColaboradorEpi> ColaboradoresEpis { get; set; }
    }
}