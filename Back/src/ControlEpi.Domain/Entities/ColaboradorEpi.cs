using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ControlEpi.Domain.Entities
{
    public class ColaboradorEpi
    {
        [Key]
        public int Id { get; set; }
        public int ColaboradorId { get; set; }
        public string ColaboradorNome { get; set; }
        public Colaborador Colaborador { get; set; }
        public int EpiId { get; set; }
        public string EpiNome { get; set; }
        public Epi Epi { get; set; }
        public DateTime DataHoraEntrega { get; set; }
        public bool Devolvido { get; set; }
    }
}