using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControlEpi.Domain.Entities.PageList
{
    public class PageParams
    {
        private int pageSize = 10;        
        public const int MaxPageSize = 50;        
        public int PageNumber { get; set; } = 1;        
        public int PageSize 
        { 
            get { return pageSize; } 
            set { pageSize = (value > MaxPageSize) ? MaxPageSize : value; }
        }
        public string Term { get; set; } = string.Empty;
    }
}