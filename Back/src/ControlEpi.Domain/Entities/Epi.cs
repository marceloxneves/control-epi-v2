using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControlEpi.Domain.Entities
{
    public class Epi
    {
        public int Id { get; set; }
        public string CodigoInterno { get; set; }
        public string Nome { get; set; }
        public string Marca { get; set; }
        public decimal Custo { get; set; }
        public int QtdeEstoque { get; set; }
        public int PrazoValidadeDias { get; set; }
        public string Foto { get; set; }
        public IEnumerable<ColaboradorEpi> ColaboradoresEpis { get; set; }
    }
}