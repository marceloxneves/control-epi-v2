using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Application.Interfaces;
using ControlEpi.Application.Dtos;
using ControlEpi.Domain.Entities;
using ControlEpi.Domain.Interfaces;
using AutoMapper;
using ControlEpi.Domain.Entities.PageList;

namespace ControlEpi.Application.Implementations
{
    public class EpiAppService : IEpiAppService
    {
        private readonly IEpiRepository _repository;
        private readonly IMapper _mapper;

        public EpiAppService(IEpiRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<EpiDto> Add(EpiDto entity)
        {
            var  entityDomain = _mapper.Map<Epi>(entity);

            _repository.Add(entityDomain);

            if(await _repository.SaveChangesAsync()){
                var id = (int)entityDomain.GetType().GetProperty("Id").GetValue(entityDomain);

                entityDomain = await _repository.GetByIdAsync(id);

                var entityView = _mapper.Map<EpiDto>(entityDomain);

                return entityView;
            }

            return entity;
        }

        public async Task<bool> Delete(int id)
        {
            var entity = await _repository.GetByIdAsync(id);

            _repository.Delete(entity);

            if(await _repository.SaveChangesAsync()){
                return true;
            }

            return false;
        }

        public async Task<bool> DeleteRange(IEnumerable<EpiDto> entities)
        {
            var  entitiesDomain = _mapper.Map<IEnumerable<Epi>>(entities);

            _repository.DeleteRange(entitiesDomain);

            if(await _repository.SaveChangesAsync()){
                return true;
            }

            return false;
        }

        public async Task<EpiDto> Update(int id, EpiDto entity)
        {
            var epi = await _repository.GetByIdAsync(id);

            if(epi == null) return null;

            entity.Id = epi.Id;

            _mapper.Map(entity, epi);

            _repository.Update(epi);

            if(await _repository.SaveChangesAsync()){
                var epiRetorno = await _repository.GetByIdAsync(epi.Id);

                var epiView = _mapper.Map<EpiDto>(epiRetorno);
                
                return epiView;
            }

            return null;
        }

        public async Task<PageList<EpiDto>> GetAllAsync(PageParams pageParams)
        {
            var epis = await _repository.GetAllAsync(pageParams);

            if(epis == null) return null;

            var result = _mapper.Map<PageList<EpiDto>>(epis);

            result.CurrentPage = epis.CurrentPage;
            result.TotalPages = epis.TotalPages;
            result.PageSize = epis.PageSize;
            result.TotalCount = epis.TotalCount;

            return result;
        }

        public async Task<EpiDto> GetByIdAsync(int id)
        {
            var epi = await _repository.GetByIdAsync(id);

            if(epi == null) return null;

            var result = _mapper.Map<EpiDto>(epi);

            return result;
        }
    }
}