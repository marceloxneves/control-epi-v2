using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Application.Interfaces;
using ControlEpi.Application.Dtos;
using ControlEpi.Domain.Entities;
using ControlEpi.Domain.Interfaces;
using AutoMapper;
using ControlEpi.Domain.Entities.PageList;

namespace ControlEpi.Application.Implementations
{
    public class EmpresaAppService : IEmpresaAppService
    {
        private readonly IEmpresaRepository _repository;
        private readonly IMapper _mapper;

        public EmpresaAppService(IEmpresaRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<EmpresaDto> Add(EmpresaDto entity)
        {
            var  entityDomain = _mapper.Map<Empresa>(entity);

            _repository.Add(entityDomain);

            if(await _repository.SaveChangesAsync()){
                var id = (int)entityDomain.GetType().GetProperty("Id").GetValue(entityDomain);

                entityDomain = await _repository.GetByIdAsync(id);

                var entityView = _mapper.Map<EmpresaDto>(entityDomain);

                return entityView;
            }

            return entity;
        }

        public async Task<bool> Delete(int id)
        {
            var entity = await _repository.GetByIdAsync(id);

            _repository.Delete(entity);

            if(await _repository.SaveChangesAsync()){
                return true;
            }

            return false;
        }

        public async Task<bool> DeleteRange(IEnumerable<EmpresaDto> entities)
        {
            var  entitiesDomain = _mapper.Map<IEnumerable<Empresa>>(entities);

            _repository.DeleteRange(entitiesDomain);

            if(await _repository.SaveChangesAsync()){
                return true;
            }

            return false;
        }

        public async Task<EmpresaDto> Update(int id, EmpresaDto entity)
        {
            var empresa = await _repository.GetByIdAsync(id);

            if(empresa == null) return null;

            entity.Id = empresa.Id;

            _mapper.Map(entity, empresa);

            _repository.Update(empresa);

            if(await _repository.SaveChangesAsync()){
                var empresaRetorno = await _repository.GetByIdAsync(empresa.Id);

                var empresaView = _mapper.Map<EmpresaDto>(empresaRetorno);
                
                return empresaView;
            }

            return null;
        }

        public async Task<PageList<EmpresaDto>> GetAllAsync(PageParams pageParams)
        {
            var empresas = await _repository.GetAllAsync(pageParams);

            if(empresas == null) return null;

            var result = _mapper.Map<PageList<EmpresaDto>>(empresas);

            result.CurrentPage = empresas.CurrentPage;
            result.TotalPages = empresas.TotalPages;
            result.PageSize = empresas.PageSize;
            result.TotalCount = empresas.TotalCount;

            return result;
        }

        public async Task<EmpresaDto> GetByIdAsync(int id)
        {
            var empresa = await _repository.GetByIdAsync(id);

            if(empresa == null) return null;

            var result = _mapper.Map<EmpresaDto>(empresa);

            return result;
        }

        public async Task<EmpresaDto> GetUnicaAsync()
        {
            var empresa = await _repository.GetUnicaAsync();

            if(empresa == null) return null;

            var result = _mapper.Map<EmpresaDto>(empresa);

            return result;
        }
    }
}