using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Application.Interfaces;
using ControlEpi.Application.Dtos;
using ControlEpi.Domain.Entities;
using ControlEpi.Domain.Interfaces;
using AutoMapper;
using ControlEpi.Domain.Entities.PageList;

namespace ControlEpi.Application.Implementations
{
    public class SetorAppService : ISetorAppService
    {
        private readonly ISetorRepository _repository;
        private readonly IMapper _mapper;

        public SetorAppService(ISetorRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<SetorDto> Add(SetorDto entity)
        {
            var  entityDomain = _mapper.Map<Setor>(entity);

            _repository.Add(entityDomain);

            if(await _repository.SaveChangesAsync()){
                var id = (int)entityDomain.GetType().GetProperty("Id").GetValue(entityDomain);

                entityDomain = await _repository.GetByIdAsync(id);

                var entityView = _mapper.Map<SetorDto>(entityDomain);

                return entityView;
            }

            return entity;
        }

        public async Task<bool> Delete(int id)
        {
            var entity = await _repository.GetByIdAsync(id);

            _repository.Delete(entity);

            if(await _repository.SaveChangesAsync()){
                return true;
            }

            return false;
        }

        public async Task<bool> DeleteRange(IEnumerable<SetorDto> entities)
        {
            var  entitiesDomain = _mapper.Map<IEnumerable<Setor>>(entities);

            _repository.DeleteRange(entitiesDomain);

            if(await _repository.SaveChangesAsync()){
                return true;
            }

            return false;
        }

        public async Task<SetorDto> Update(int id, SetorDto entity)
        {
            var setor = await _repository.GetByIdAsync(id);

            if(setor == null) return null;

            entity.Id = setor.Id;

            _mapper.Map(entity, setor);

            _repository.Update(setor);

            if(await _repository.SaveChangesAsync()){
                var setorRetorno = await _repository.GetByIdAsync(setor.Id);

                var setorView = _mapper.Map<SetorDto>(setorRetorno);
                
                return setorView;
            }

            return null;
        }

        public async Task<PageList<SetorDto>> GetAllAsync(PageParams pageParams)
        {
            var setores = await _repository.GetAllAsync(pageParams);
            
            if(setores == null) return null;
            
            var result = _mapper.Map<PageList<SetorDto>>(setores);

            result.CurrentPage = setores.CurrentPage;
            result.TotalPages = setores.TotalPages;
            result.PageSize = setores.PageSize;
            result.TotalCount = setores.TotalCount;

            return result;
        }

        public async Task<SetorDto> GetByIdAsync(int id)
        {
            var setor = await _repository.GetByIdAsync(id);
            
            if(setor == null) return null;

            var result = _mapper.Map<SetorDto>(setor);

            return result;
        }
    }
}