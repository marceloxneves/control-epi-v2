using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ControlEpi.Application.Dtos;
using ControlEpi.Application.Interfaces;
using ControlEpi.Domain.Entities;
using ControlEpi.Domain.Interfaces;

namespace ControlEpi.Application.Implementations
{
    public class ColaboradorEpiAppService : IColaboradorEpiAppService
    {
        private readonly IColaboradorEpiRepository _repository;
        private readonly IColaboradorRepository colaboradorRepository;
        private readonly IEpiRepository epiRepository;
        private readonly IMapper _mapper;


        public ColaboradorEpiAppService(
            IColaboradorEpiRepository repository, 
            IColaboradorRepository colaboradorRepository,
            IEpiRepository epiRepository,
            IMapper mapper)
        {
            this.epiRepository = epiRepository;
            this.colaboradorRepository = colaboradorRepository;
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<bool> DeleteColaboradorEpi(int id)
        {
            var entity = await _repository.GetByIdAsync(id);

            //_repository.Delete(entity);     
            entity.Devolvido = true;       

            if(await _repository.SaveChangesAsync()){
                return true;
            }

            return false;
        }

        public async Task<ColaboradorEpiDto[]> GetColaboradorEpiByColaboradorIdAsync(int colaboradorId)
        {
            try
            {
                 var ces = await _repository.GetColaboradorEpiByColaboradorIdAsync(colaboradorId);

                 if(ces == null) return null;

                 var result = _mapper.Map<ColaboradorEpiDto[]>(ces);

                 return result;
            }
            catch (System.Exception ex)
            {                
                throw new FieldAccessException(ex.Message);
            }
        }

        public async Task<ColaboradorEpiDto> GetByIdAsync(int id)
        {
            try
            {
                 var ce = await _repository.GetByIdAsync(id);

                 if(ce == null) return null;

                 var result = _mapper.Map<ColaboradorEpiDto>(ce);

                 return result;
            }
            catch (System.Exception ex)
            {                
                throw new FieldAccessException(ex.Message);
            }
        }

        private async Task Add(int colaboradorId, ColaboradorEpiDto entity)
        {
            var  entityDomain = _mapper.Map<ColaboradorEpi>(entity);
            entityDomain.ColaboradorId = colaboradorId;

            _repository.Add(entityDomain);

            await _repository.SaveChangesAsync();

            entityDomain = await _repository.GetByIdAsync(entityDomain.Id);

            var entityView = _mapper.Map<ColaboradorEpiDto>(entityDomain);
        }

        public async Task<ColaboradorEpiDto[]> SaveColaboradoresEpis(int colaboradorId, int epiId, ColaboradorEpiDto[] models)
        {
            try
            {
                //var ce = _mapper.Map<ColaboradorEpi>(model);

                //ce.DataHoraEntrega = DateTime.Now;

                //_repository.Add(ce);

                //await _repository.SaveChangesAsync();

                //var cesResult = await _repository.GetColaboradorEpiByColaboradorIdAsync(ce.ColaboradorId);

                //return _mapper.Map<ColaboradorEpiDto[]>(cesResult);  
                var ces = await _repository.GetColaboradorEpiByColaboradorIdAsync(colaboradorId);

                if (ces == null) return null;

                foreach (var model in models)
                {                                            
                    if (model.Id == 0)
                    {     
                        var colaborador = await colaboradorRepository.GetByIdAsync(colaboradorId);
                        var epi = await epiRepository.GetByIdAsync(epiId);
                        var ce = _mapper.Map<ColaboradorEpi>(model);

                        ce.ColaboradorId = colaboradorId;
                        ce.EpiId = epiId;
                        ce.DataHoraEntrega = DateTime.Now;
                        ce.ColaboradorNome = colaborador.Nome;
                        ce.EpiNome = epi.Nome;

                        _repository.Add(ce);

                        await _repository.SaveChangesAsync();
                    }
                }

                var cesRetorno = await _repository.GetColaboradorEpiByColaboradorIdAsync(colaboradorId);

                return _mapper.Map<ColaboradorEpiDto[]>(cesRetorno); 
            }
            catch (System.Exception ex)
            {                
                throw new FieldAccessException(ex.Message);
            }
        }

        public async Task<ColaboradorEpiDto[]> SaveColaboradorEpis(int colaboradorId, ColaboradorEpiDto model)
        {
            try
            {
                var colaborador = await colaboradorRepository.GetByIdAsync(model.ColaboradorId);
                var epi = await epiRepository.GetByIdAsync(model.EpiId);
                var ce = _mapper.Map<ColaboradorEpi>(model);

                ce.ColaboradorId = colaboradorId;
                ce.DataHoraEntrega = DateTime.Now;
                ce.ColaboradorNome = colaborador.Nome;
                ce.EpiNome = epi.Nome;

                _repository.Add(ce);

                await _repository.SaveChangesAsync();

                _repository.Add(ce);

                await _repository.SaveChangesAsync();

                var cesResult = await _repository.GetColaboradorEpiByColaboradorIdAsync(ce.ColaboradorId);

                return _mapper.Map<ColaboradorEpiDto[]>(cesResult);  
                
            }
            catch (System.Exception ex)
            {                
                throw new FieldAccessException(ex.Message);
            }
        }

        public async Task<bool> GiveBackColaboradorEpi(int id)
        {
            try
            {
                 var ce = await _repository.GetByIdAsync(id);

                 if(ce == null) return false;

                 ce.Devolvido = true;

                 await _repository.SaveChangesAsync();

                 return true;
            }
            catch (System.Exception)
            {                
                return false;
            }
        }
    }
}