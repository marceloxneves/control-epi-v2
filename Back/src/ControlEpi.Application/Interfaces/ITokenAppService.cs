using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Application.Dtos;

namespace ControlEpi.Application.Interfaces
{
    public interface ITokenAppService
    {
        Task<string> CreateToken(UserUpdateDto userUpdateDto);
    }
}