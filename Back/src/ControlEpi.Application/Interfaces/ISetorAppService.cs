using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Domain.Entities;
using ControlEpi.Application.Dtos;
using ControlEpi.Domain.Entities.PageList;

namespace ControlEpi.Application.Interfaces
{
    public interface ISetorAppService
    {
        Task<SetorDto> Add(SetorDto entity);
        Task<SetorDto> Update(int id, SetorDto entity);
        Task<bool> Delete(int id);
        Task<bool> DeleteRange(IEnumerable<SetorDto> entities);
        Task<PageList<SetorDto>> GetAllAsync(PageParams pageParams);
        Task<SetorDto> GetByIdAsync(int id);
    }
}