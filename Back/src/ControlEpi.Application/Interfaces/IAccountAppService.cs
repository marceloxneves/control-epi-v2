using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Application.Dtos;
using Microsoft.AspNetCore.Identity;

namespace ControlEpi.Application.Interfaces
{
    public interface IAccountAppService
    {
        Task<bool> UserExists(string username);
        Task<UserUpdateDto> GetUserByUserNameAsync(string username);
        Task<SignInResult> CheckUserPasswordAsync(UserUpdateDto userUpdateDto, string password);
        Task<UserUpdateDto> CreateAccountAsync(UserDto userDto);
        Task<UserUpdateDto> UpdateAccount(UserUpdateDto userUpdateDto);
    }
}