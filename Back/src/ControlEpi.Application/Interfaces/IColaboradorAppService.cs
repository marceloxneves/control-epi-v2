using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Domain.Entities;
using ControlEpi.Application.Dtos;
using ControlEpi.Domain.Entities.PageList;

namespace ControlEpi.Application.Interfaces
{
    public interface IColaboradorAppService
    {
        Task<ColaboradorDto> Add(ColaboradorDto entity);
        Task<ColaboradorDto> Update(int id, ColaboradorDto entity);
        Task<bool> Delete(int id);
        Task<bool> DeleteRange(IEnumerable<ColaboradorDto> entities);
        Task<PageList<ColaboradorDto>> GetAllAsync(PageParams pageParams);
        Task<ColaboradorDto> GetByIdAsync(int id);
    }
}