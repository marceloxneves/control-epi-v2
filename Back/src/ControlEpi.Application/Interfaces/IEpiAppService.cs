using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Domain.Entities;
using ControlEpi.Application.Dtos;
using ControlEpi.Domain.Entities.PageList;

namespace ControlEpi.Application.Interfaces
{
    public interface IEpiAppService
    {
        Task<EpiDto> Add(EpiDto entity);
        Task<EpiDto> Update(int id, EpiDto entity);
        Task<bool> Delete(int id);
        Task<bool> DeleteRange(IEnumerable<EpiDto> entities);
        Task<PageList<EpiDto>> GetAllAsync(PageParams pageParams);
        Task<EpiDto> GetByIdAsync(int id);
    }
}