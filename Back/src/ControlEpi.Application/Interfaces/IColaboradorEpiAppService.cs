using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Application.Dtos;

namespace ControlEpi.Application.Interfaces
{
    public interface IColaboradorEpiAppService
    {
        //Task Add(int colaboradorId, ColaboradorEpiDto entity);
        Task<ColaboradorEpiDto[]> SaveColaboradoresEpis(int colaboradorId, int epiId, ColaboradorEpiDto[] models);
        Task<ColaboradorEpiDto[]> SaveColaboradorEpis(int colaboradorId, ColaboradorEpiDto models);
        Task<bool> DeleteColaboradorEpi(int id);
        Task<bool> GiveBackColaboradorEpi(int id);
         Task<ColaboradorEpiDto[]> GetColaboradorEpiByColaboradorIdAsync(int colaboradorId);
        Task<ColaboradorEpiDto> GetByIdAsync(int id);
    }
}