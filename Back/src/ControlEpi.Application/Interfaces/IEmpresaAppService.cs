using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Domain.Entities;
using ControlEpi.Application.Dtos;
using ControlEpi.Domain.Entities.PageList;

namespace ControlEpi.Application.Interfaces
{
    public interface IEmpresaAppService
    {
        Task<EmpresaDto> Add(EmpresaDto entity);
        Task<EmpresaDto> Update(int id, EmpresaDto entity);
        Task<bool> Delete(int id);
        Task<bool> DeleteRange(IEnumerable<EmpresaDto> entities);
        Task<PageList<EmpresaDto>> GetAllAsync(PageParams pageParams);
        Task<EmpresaDto> GetByIdAsync(int id);
        Task<EmpresaDto> GetUnicaAsync();
    }
}