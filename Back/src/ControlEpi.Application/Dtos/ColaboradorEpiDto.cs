using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControlEpi.Application.Dtos
{
    public class ColaboradorEpiDto
    {
        public int Id { get; set; }
        public int ColaboradorId { get; set; }
        public string ColaboradorNome { get; set; }
        public ColaboradorDto ColaboradorDto { get; set; }
        public int EpiId { get; set; }
        public string EpiNome { get; set; }
        public EpiDto EpiDto { get; set; }
        public DateTime DataHoraEntrega { get; set; }
        public bool Devolvido { get; set; }
    }
}