using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ControlEpi.Application.Dtos
{
    public class ColaboradorDto
    {
        public int Id { get; set; }
        public int SetorId { get; set; }
        //public SetorDto SetorDto { get; set;}
        //[Required(ErrorMessage = "{0} Obrigatório")]        
        //[StringLength(200, MinimumLength = 3, ErrorMessage = "{0} deve ter entre 3 e 200 caracteres")]
        //outra forma de fazer esta mesma validação
        //[MinLength(3, ErrorMessage = "{0} deve ter no mínimo 3 caracteres"), MaxLength(200, ErrorMessage = "{0} deve ter no máximo 200 caracteres")]
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public string Rg { get; set; }
        public string Endereco { get; set; }
        public string Municipio { get; set; }
        public string Uf { get; set; }
        public string Cep { get; set; }
        //[Phone(ErrorMessage = "{0} possui Número inválido")]
        public string Telefone { get; set; }
        [Display(Name = "E-Mail")]
        //[EmailAddress(ErrorMessage = "{0} não é um endereço de e-mail válido")]
        public string Email { get; set; }
        //[RegularExpression(@".*\.(gif|jpe?g|bmp|png)$", ErrorMessage = "Formato inválido (.gif, .jpeg, .bmp, .png)")]
        //public string Foto { get; set; }
        public IEnumerable<ColaboradorEpiDto> ColaboradoresEpisDto { get; set; }
    }
}