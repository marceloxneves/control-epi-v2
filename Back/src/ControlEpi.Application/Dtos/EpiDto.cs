using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ControlEpi.Application.Dtos
{
    public class EpiDto
    {
        public int Id { get; set; }
        public string CodigoInterno { get; set; }
        public string Nome { get; set; }
        public string Marca { get; set; }
        public decimal Custo { get; set; }
        public int PrazoValidadeDias { get; set; }
        [Range(1, 1000, ErrorMessage = "{0} deve ser entre 1 e 1.000")]
        public int QtdeEstoque { get; set; }
        public string Foto { get; set; }
        public IEnumerable<ColaboradorEpiDto> ColaboradoresEpisDto { get; set; }
    }
}