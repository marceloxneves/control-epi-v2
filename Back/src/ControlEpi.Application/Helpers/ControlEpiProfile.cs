using System;
using AutoMapper;
using ControlEpi.Domain.Entities;
using ControlEpi.Application.Dtos;
using ControlEpi.Domain.Identity;

namespace ControlEpi.Application.Helpers
{
    public class ControlEpiProfile : Profile
    {
        public ControlEpiProfile()
        {
            CreateMap<Colaborador, ColaboradorDto>()
                //.ForMember(d => d.SetorDto, c => c.MapFrom(m => m.Setor))
                .ForMember(d => d.ColaboradoresEpisDto, c => c.MapFrom(m => m.ColaboradoresEpis))
                .ReverseMap();
            CreateMap<ColaboradorEpi, ColaboradorEpiDto>()
                .ForMember(d => d.ColaboradorDto, c => c.MapFrom(m => m.Colaborador))
                .ForMember(d => d.EpiDto, c => c.MapFrom(m => m.Epi))
                .ReverseMap();
            CreateMap<Empresa, EmpresaDto>().ReverseMap();
            CreateMap<Epi, EpiDto>()
                .ForMember(d => d.ColaboradoresEpisDto, c => c.MapFrom(m => m.ColaboradoresEpis))
                .ReverseMap();
            CreateMap<Setor, SetorDto>().ReverseMap();

            CreateMap<User, UserDto>().ReverseMap();
            CreateMap<User, UserLoginDto>().ReverseMap();
            CreateMap<User, UserUpdateDto>().ReverseMap();
        }
    }
}