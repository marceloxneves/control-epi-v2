using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.API.Extensions;
using ControlEpi.Application.Dtos;
using ControlEpi.Application.Interfaces;
using ControlEpi.Domain.Entities;
using ControlEpi.Domain.Entities.PageList;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ControlEpi.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class EmpresaController : ControllerBase
    {
        private readonly IEmpresaAppService _appService;

        public EmpresaController(IEmpresaAppService appService)
        {
            _appService = appService;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery]PageParams pageParams)
        {
            var empresas = await _appService.GetAllAsync(pageParams);

            if(empresas == null)
            {
                return NotFound("Nenhuma empresa encontrada");
            }

            Response.AddPagination(empresas.CurrentPage, empresas.PageSize, empresas.TotalCount, empresas.TotalPages);

            return Ok(empresas);    
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var empresa = await _appService.GetByIdAsync(id);

            if(empresa == null)
            {
                return NotFound("Empresa não encontrada");
            }

            return Ok(empresa);    
        }

        [HttpGet]
        [Route("get-unica")]
        public async Task<IActionResult> GetUnica()
        {
            var empresa = await _appService.GetUnicaAsync();

            if(empresa == null)
            {
                return NotFound("Empresa não encontrada");
            }

            return Ok(empresa);    
        }  

        // [HttpGet("{razaoSocial}/razaoSocial")]
        // public async Task<IActionResult> GetByRazaoSocial(string razaoSocial)
        // {
        //     var empresa = await _appService.GetByRazaoSocialAsync(razaoSocial);

        //     if(empresa == null)
        //     {
        //         return NotFound("Empresa não encontrada");
        //     }

        //     return Ok(empresa);    
        // } 

        [HttpPost]
        public async Task<IActionResult> Post(EmpresaDto empresa)
        {
            var empresaNew = await _appService.Add(empresa);

            if(empresaNew == null)
            {
                return NotFound("Empresa não encontrada");
            }

            return Ok(empresaNew);    
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, EmpresaDto empresa)
        {
            var empresaAlt = await _appService.Update(id, empresa);

            if(empresaAlt == null)
            {
                return NotFound("Empresa não encontrada");
            }

            return Ok(empresaAlt);    
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                 if(await _appService.Delete(id))
                 {
                    return Ok("Deletado com Sucesso!");
                 }
                 else
                 {
                    return BadRequest("Falha ao tentar deleção.");
                 }
            }
            catch (System.Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Erro: { ex.Message }");
            } 
        }
    }
}