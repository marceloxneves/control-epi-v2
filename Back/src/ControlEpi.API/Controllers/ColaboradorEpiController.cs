using System.Threading.Tasks;
using ControlEpi.Application.Dtos;
using ControlEpi.Application.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ControlEpi.API.Controllers
{    
    [ApiController]
    [Route("api/[controller]")]
    public class ColaboradorEpiController : ControllerBase
    {
        private readonly IColaboradorEpiAppService _colaboradorEpiApp;

        public ColaboradorEpiController(IColaboradorEpiAppService colaboradorEpiApp)
        {
            _colaboradorEpiApp = colaboradorEpiApp;
        }

        [HttpGet("{colaboradorId}")]
        public async Task<IActionResult> Get(int colaboradorId)
        {
            try
            {
                 var ces = await _colaboradorEpiApp.GetColaboradorEpiByColaboradorIdAsync(colaboradorId);

                if(ces == null)
                {
                    return NotFound("EPI's não encontrados");
                }

                return Ok(ces); 
            }
            catch (System.Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, $"Falha ao recuperar EPI's. Erro: { ex.Message }");
            }
        }

        [HttpPost("{colaboradorId}/{epiId}")]
        public async Task<IActionResult> Post(int colaboradorId, int epiId, ColaboradorEpiDto[] model)
        {
            var ces = await _colaboradorEpiApp.SaveColaboradoresEpis(colaboradorId, epiId, model);

            if(ces == null)
            {
                return NoContent();
            }

            return Ok(ces);    
        }    
        // [HttpPost("{colaboradorId}")]
        // public async Task<IActionResult> Post(int colaboradorId, ColaboradorEpiDto model)
        // {
        //     var ces = await _colaboradorEpiApp.SaveColaboradorEpis(colaboradorId, model);

        //     if(ces == null)
        //     {
        //         return NoContent();
        //     }

        //     return Ok(ces);    
        // }    

        [HttpDelete("{id}/{devolucao}")]
        public async Task<IActionResult> Delete(int id, bool devolucao = false)
        {
            try
            {
                var ce = _colaboradorEpiApp.GetByIdAsync(id);

                if(ce == null) return NoContent();
                
                if (devolucao){
                    return await _colaboradorEpiApp.DeleteColaboradorEpi(id)
                        ? Ok(true)
                        : BadRequest("Falha");
                }

                 return await _colaboradorEpiApp.DeleteColaboradorEpi(id)
                 ? Ok(true)
                 : BadRequest("Falha");
            }
            catch (System.Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Erro: { ex.Message }");
            } 
        }
    }
}