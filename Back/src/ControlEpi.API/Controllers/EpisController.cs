using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.API.Extensions;
using ControlEpi.API.Helpers;
using ControlEpi.Application.Dtos;
using ControlEpi.Application.Interfaces;
using ControlEpi.Domain.Entities;
using ControlEpi.Domain.Entities.PageList;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ControlEpi.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class EpisController : Controller
    {
        private readonly IEpiAppService _appService;
        private readonly IUtil util;
        private readonly string _destino = "Images";

        //private readonly IWebHostEnvironment _hostEnv;

        public EpisController(
            IEpiAppService appService, 
            //IWebHostEnvironment hostEnv
            IUtil util)
        {
            _appService = appService;
            this.util = util;
            //_hostEnv = hostEnv;
        }

         [HttpGet]
        public async Task<IActionResult> Get([FromQuery]PageParams pageParams)
        {
            var epis = await _appService.GetAllAsync(pageParams);

            if(epis == null)
            {
                return NotFound("Nenhum epi encontrado");
            }

            Response.AddPagination(epis.CurrentPage, epis.PageSize, epis.TotalCount, epis.TotalPages);

            return Ok(epis);    
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var epi = await _appService.GetByIdAsync(id);

            if(epi == null)
            {
                return NotFound("EPI não encontrado");
            }

            return Ok(epi);    
        }    

        // [HttpGet("{name}/name")]
        // public async Task<IActionResult> GetByName(string name)
        // {
        //     var epi = await _appService.GetByNameAsync(name);

        //     if(epi == null)
        //     {
        //         return NotFound("EPI não encontrado");
        //     }

        //     return Ok(epi);    
        // } 

        [HttpPost]
        public async Task<IActionResult> Post(EpiDto epi)
        {
            try
            {
                 var epiNew = await _appService.Add(epi);

                if(epiNew == null)
                {
                    return NotFound("EPI não encontrado");
                }

                return Ok(epiNew);  
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, $"Falha ao atualizar EPI. Erro: {ex.Message}");
            }
              
        }

        [HttpPost("upload-foto-epi/{epiId}")]
        public async Task<IActionResult> UploadPhoto(int epiId)
        {
            try
            {
                 var epi = await _appService.GetByIdAsync(epiId);
            
            if(epi == null) return NoContent();

            var file = Request.Form.Files[0];

            if(file.Length > 0){
                util.DeleteImage(epi.Foto, _destino);
                epi.Foto = await util.SaveImage(file, _destino);
            }

            var epiRetorno = await _appService.Update(epiId, epi);

                return Ok(epiRetorno);
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError,
                    $"Erro ao tentar realizar upload de Foto do EPI. Erro: {ex.Message}");
            }                
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, EpiDto epi)
        {
            var epiAlt = await _appService.Update(id, epi);

            if(epiAlt == null)
            {
                return NotFound("EPI não encontrado");
            }

            return Ok(epiAlt);    
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var epi = await _appService.GetByIdAsync(id);

                if(epi == null) return NotFound("Epi Não Encontrado");

                 if(await _appService.Delete(id))
                 {
                    util.DeleteImage(epi.Foto, _destino);

                    return Ok("Deletado com Sucesso!");
                 }
                 else
                 {
                    return BadRequest("Falha ao tentar deleção.");
                 }
            }
            catch (System.Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Erro: { ex.Message }");
            } 
        } 

        // [NonAction]
        // public async Task<string> SaveImage(IFormFile imageFile)
        // {
        //     string imageName = new String(Path.GetFileNameWithoutExtension(imageFile.FileName).Take(10).ToArray()).Replace(' ','-');

        //     imageName = $"{imageName}{DateTime.UtcNow.ToString("yymmssfff")}{Path.GetExtension(imageFile.FileName)}";

        //     var imagePath = Path.Combine(_hostEnv.ContentRootPath, @"Resources/images", imageName);

        //     using(var fileStream = new FileStream(imagePath, FileMode.Create))
        //     {
        //         await imageFile.CopyToAsync(fileStream);
        //     }

        //     return imageName;
        // }

        // [NonAction]
        // public void DeleteImage(string imageName)
        // {
        //     var imagePath = Path.Combine(_hostEnv.ContentRootPath, @"Resources/images", imageName);

        //     if(System.IO.File.Exists(imagePath)){
        //         System.IO.File.Delete(imagePath);
        //     }
        // }
    }
}