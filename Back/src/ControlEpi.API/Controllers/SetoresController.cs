using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.API.Extensions;
using ControlEpi.Application.Dtos;
using ControlEpi.Application.Interfaces;
using ControlEpi.Domain.Entities;
using ControlEpi.Domain.Entities.PageList;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ControlEpi.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class SetoresController : ControllerBase
    {
        private readonly ISetorAppService _appService;

        public SetoresController(ISetorAppService appService)
        {
            _appService = appService;
        } 

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery]PageParams pageParams)
        {
            var setores = await _appService.GetAllAsync(pageParams);

            if(setores == null)
            {
                return NotFound("Nenhum setor encontrado");
            }

            Response.AddPagination(setores.CurrentPage, setores.PageSize, setores.TotalCount, setores.TotalPages);

            return Ok(setores);    
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                 var setor = await _appService.GetByIdAsync(id);

                if(setor == null)
                {
                    return NotFound("Setor não encontrado");
                }

                return Ok(setor); 
            }
            catch (System.Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, $"Falha ao recuperar setores. Erro: { ex.Message }");
            }
               
        }    

        // [HttpGet("{name}/name")]
        // public async Task<IActionResult> GetByName(string name)
        // {
        //     var setor = await _appService.GetByNameAsync(name);

        //     if(setor == null)
        //     {
        //         return NotFound("Setor não encontrado");
        //     }

        //     return Ok(setor);    
        // } 

        [HttpPost]
        public async Task<IActionResult> Post(SetorDto setor)
        {
            var setorNew = await _appService.Add(setor);

            if(setorNew == null)
            {
                return NotFound("Setor não encontrado");
            }

            return Ok(setorNew);    
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, SetorDto setor)
        {
            var setorAlt = await _appService.Update(id, setor);

            if(setorAlt == null)
            {
                return NotFound("Setor não encontrado");
            }

            return Ok(setorAlt);    
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                 if(await _appService.Delete(id))
                 {
                    return Ok(new { message = "Deletado"});
                 }
                 else
                 {
                    return BadRequest("Falha ao tentar deleção.");
                 }
            }
            catch (System.Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Erro: { ex.Message }");
            } 
        }       
    }
}