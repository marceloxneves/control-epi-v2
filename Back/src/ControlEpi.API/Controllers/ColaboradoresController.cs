using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.API.Extensions;
using ControlEpi.Application.Dtos;
using ControlEpi.Application.Interfaces;
using ControlEpi.Domain.Entities;
using ControlEpi.Domain.Entities.PageList;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ControlEpi.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class ColaboradoresController : ControllerBase
    {
        private readonly IColaboradorAppService _appService;

        public ColaboradoresController(IColaboradorAppService appService)
        {
            _appService = appService;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery]PageParams pageParams)
        {
            var colaboradores = await _appService.GetAllAsync(pageParams);

            if(colaboradores == null)
            {
                return NoContent();
            }

            Response.AddPagination(colaboradores.CurrentPage, colaboradores.PageSize, colaboradores.TotalCount, colaboradores.TotalPages);

            return Ok(colaboradores);    
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var colaborador = await _appService.GetByIdAsync(id);

            if(colaborador == null)
            {
                return NoContent();
            }

            return Ok(colaborador);    
        }    

        // [HttpGet("{name}/name")]
        // public async Task<IActionResult> GetByName(string name)
        // {
        //     var colaborador = await _appService.GetByNameAsync(name);

        //     if(colaborador == null)
        //     {
        //         return NoContent();
        //     }

        //     return Ok(colaborador);    
        // } 

        [HttpPost]
        public async Task<IActionResult> Post(ColaboradorDto colaborador)
        {
            var colaboradorNew = await _appService.Add(colaborador);

            if(colaboradorNew == null)
            {
                return BadRequest();
            }

            return Ok(colaboradorNew);    
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, ColaboradorDto colaborador)
        {
            var colaboradorAlt = await _appService.Update(id, colaborador);

            if(colaboradorAlt == null)
            {
                return NoContent();
            }

            return Ok(colaboradorAlt);    
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                 if(await _appService.Delete(id))
                 {
                    return Ok("Deletado com Sucesso!");
                 }
                 else
                 {
                    return BadRequest("Falha ao tentar deleção.");
                 }
            }
            catch (System.Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Erro: { ex.Message }");
            } 
        }
    }
}